package org.example;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void adicao() {
        double resultado = Calculator.adicao(5.0, 3.0);
        assertEquals(8.0, resultado, 0.0001);
    }

    @org.junit.jupiter.api.Test
    void adicaoNegativos() {
        double resultado = Calculator.adicao(5.0, - 3.0);
        assertEquals(2.0, resultado, 0.0001);
    }
    @org.junit.jupiter.api.Test
    void subtracao() {
        double resultado = Calculator.subtracao(5.0, 3.0);
        assertEquals(2.0, resultado, 0.0001);
    }

    @org.junit.jupiter.api.Test
    void multiplicacao() {
        double resultado = Calculator.multiplicacao(5.0, 3.0);
        assertEquals(15.0, resultado, 0.0001);
    }

    @org.junit.jupiter.api.Test
    void multiplicacaoPorZero() {
        double resultado = Calculator.multiplicacao(5.0, 0.0);
        assertEquals(0.0, resultado, 0.0001);
    }

    @org.junit.jupiter.api.Test
    void divisao() {
        double resultado = Calculator.divisao(6.0, 3.0);
        assertEquals(2.0, resultado, 0.0001);
    }

    @org.junit.jupiter.api.Test
    void divisaoPorZero() {
        double resultado = Calculator.divisao(5.0, 0.0);
        assertTrue(Double.isInfinite(resultado));
    }
}