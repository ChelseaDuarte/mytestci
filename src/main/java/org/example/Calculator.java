package org.example;
import java.util.Scanner;


public class Calculator {

    public static double adicao(double numero1, double numero2) {
        return numero1 + numero2;
    }

    public static double subtracao(double numero1, double numero2) {
        return numero1 - numero2;
    }

    public static double multiplicacao(double numero1, double numero2) {
        return numero1 * numero2;
    }

    public static double divisao(double numero1, double numero2) {
        return numero1 / numero2;
    }

    public static void Menu() {
        System.out.println("");
        System.out.println("Que operação deseja efetuar?");
        System.out.println("1 - Adição");
        System.out.println("2 - Subtração");
        System.out.println("3 - Multiplicação");
        System.out.println("4 - Divisão");
        System.out.println("0 - Sair");
        System.out.println("");
    }

    public static double operacao(int escolha, double numero1, double numero2) {
        double resultado = 0.0;
        switch (escolha) {
            case 1:
                resultado = adicao(numero1, numero2);
                break;
            case 2:
                resultado = subtracao(numero1, numero2);
                break;
            case 3:
                resultado = multiplicacao(numero1, numero2);
                break;
            case 4:
                if (numero2 != 0) {
                    resultado = divisao(numero1, numero2);
                } else {
                    System.out.println("Erro: Divisão por zero.");
                }
                break;
            default:
                System.out.println("Opção inválida. Tente novamente.");
        }
        return resultado;
    }



    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("===================================");
            System.out.println("Calculadora CI - Chelsea Duarte");
            System.out.println("===================================");

            while (true) {
                Menu();
                int escolha = scanner.nextInt();

                if (escolha == 0) {
                    System.out.println("A sair da calculadora. Obrigado!");
                    break;
                }

                System.out.print("Insira o primeiro número: ");
                double numero1 = scanner.nextDouble();

                System.out.print("Insira o segundo número: ");
                double numero2 = scanner.nextDouble();

                double resultado = operacao(escolha, numero1, numero2);
                System.out.println("Resultado: " + resultado);
            }

            scanner.close();
        }



    }





